# example:
# -154.20 = "-154.20"
# 1000 => "1,000"
# -1000 => "-1,000"
# 10000000 => "10,000,000"

string convert(float n)
{
    string num = to_string(n);
    int index = num.find('.');
    string result = "";
    int end;
    if(string::npos != index)
    {
        end = index - 1;
        for(int i=num.size()-1; i>index; i--)
        {
            result += num[i];
        }
        result += '.';
    }else
    {
        end = num.size() - 1;
    }
    for(int i=end; i>=0; i--)
    {
        // if need to insert comma
        if((end - i) % 3 == 0)
        {
            if(i > 0 && num[i-1] != '-') // check minus sign
                result += ',';
        }
        result += num[i];
    }
    return reverse(result.begin(), result.end());
}